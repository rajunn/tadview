import pandas

'''
This script will generate the TAD database file. 
Simply run the script and you will be asked for the required input.

Required file structure:                                                                                           
    All files to be parsed (X.txt, 2R.txt, etc.) must be under the specified directory. By default, ./chromatin_Dros_melanogaster is the directory.
    The AlberModel.txt must also be under this directory.
'''





'''
Parse and Print Functions
'''
def combineChromoAndTad(chromo_df, tad_df, file):
    for tRow in tad_df.itertuples(index=True): #itertuples is fast
        tStart = tRow.StartLoc
        tEnd = tRow.EndLoc
        printList = []
        #Now we comsume the chromo_df until we reach a range above our bounds
        for cRow in chromo_df.itertuples(index=True):   
            #1 and 2 correspond to indicies for chromosome start and end location
            cStart = cRow[1]
            cEnd = cRow[2]
            #swap if backwards
            if cStart > cEnd:
                cStart,cEnd = cEnd,cStart 
            elif (cStart>= tStart and cEnd<tEnd) or (cEnd>=tStart and cStart<=tStart) or (cStart<=tEnd and cEnd>=tEnd):
                #add to list to print
                printList.append(cRow)
                chromo_df.drop(cRow[0],inplace=True)
            elif cStart>tEnd:
                break
        #Now we print stuff from printList
        printToDatabaseFile(printList,tRow,file)
    return


def printToDatabaseFile(printList, tRow, file):
    file.write(str(tRow[1])+"\t"+str(tRow[2])+"\t"+str(tRow[6])+'\t')
    printListString = ""
    for idx,value in enumerate(printList):
        if idx == 0:                                                                                               
            printListString += str(value[4])                                                                              
        else:                                                                                                      
            printListString += ","+str(value[4])
    file.write(printListString)
    relatedTadSet = tadRange.RelatedTads[int(tRow[1])-1]
    relatedTadSetString = ""
    if relatedTadSet is not None:
        for idx,value in enumerate(relatedTadSet):
            if idx == 0:
                relatedTadSetString += str(value)
            else:
                relatedTadSetString += ","+str(value)
    file.write('\t'+relatedTadSetString) #Index into tadRange to get the related tads set
    file.write('\n')                                                                                               
'''                                                                                             Main section
'''                                                                                                                
#Make pandas dataframe the TAD mapping of each chromosome
dataFolderPath = input("What is the path to the directory that contains AlberModel.txt, X.txt, 2R.txt, etc?\n./chromatin_Dros_melanogaster by default if no path entered\n")
if dataFolderPath is None or dataFolderPath is "":                                                  dataFolderPath = "./chromatin_Dros_melanogaster"                                                               
tadRange = pandas.read_csv(dataFolderPath+"/AlberModel.txt", sep='\t')                                             
fileName = input("Enter desired name for database file: \n")
fileName+=".txt"

longRangeContacts = pandas.read_csv("longRangeContacts.csv")
longRangeContacts = longRangeContacts.sort_values(by=['Chr'])

#Parse dataframe with contacts and populate RelatedTads column.
tadRange['RelatedTads'] = None
for chromo, tad_df in tadRange.groupby("Chr"):
    longRangeContactsChr = longRangeContacts[longRangeContacts["Chr"] == chromo] #Get relevant contacts
    for row in longRangeContactsChr.itertuples():
        tadsInContactX = tad_df[~((row.Xstart >= tad_df['EndLoc']) | (row.Xend < tad_df['StartLoc']))]
        tadsInContactY = tad_df[~((row.Ystart >= tad_df['EndLoc']) | (row.Yend < tad_df['StartLoc']))]
        allTadsInContact = set(tadsInContactX.DomainID) | set(tadsInContactY.DomainID)
        for i in allTadsInContact:
            tadRange.at[int(i)-1,'RelatedTads'] = allTadsInContact - set([i])

print("Working")
file = open(fileName, "a")
file.write("TAD\tChr\tEpiClass\tGenes\tRelatedTads\n") #header
for chromo, tad_df in tadRange.groupby("Chr"):
    #chromo contains the names of each chromosome
    #tad_df is a dataframe of the TAD mapping for each chromosome
    chromo_df = pandas.read_csv(dataFolderPath+"/"+chromo+".txt", sep='\t')
    #Now we step through chromo_df and tad_df and build our output
    combineChromoAndTad(chromo_df,tad_df,file)
    print(chromo+ " loaded")
file.close()
print("Completed, check "+fileName)

////////////////////////////////////////////////////////////////////
// TAD-TAD contact matrix generator				  //
// Use the Makefile to compile!					  //
// To run on "something.vtf" use:				  //
// "./tad_tad -f /path/to/something.vtf"			  //
// Output is dumped to stdout, pipe into the file of your choice. //
////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <math.h>
#include <map>


/////////////
// Classes //
/////////////

/*
  The TAD Location class represents a TAD's location in a snapshot. It contains the TAD's ID and location in xyz space. 
*/
class TadLocation
{
public:
    int num;
    double x;
    double y;
    double z;
    /*  Constructor for TAD Location class */ 
    void init(int _num, double _x, double _y, double _z)
	{
	    num = _num; //Which TAD the location is associated with
	    x = _x; //The x location
	    y = _y; //The y location
	    z = _z; //The z location
	}
};

/*
  TAD TAD Interaction Matrix
*/
class TTMatrix
{
    std::vector<double> tadVector; //Vector of all the TADs, used to determine radius.
    std::map<int,double> tadMap; //Map of tads to their radius. I do not use unordered_map because it's not supported by GCC 4.4. Thus it's O(log(n)) for all regular operations.
    std::vector<std::vector<double>> finalMat;
public:
    void init() {};
    bool contact(TadLocation a, TadLocation b);
    void printMatrix();
    void parseVTF(std::ifstream& inFile);
};


///////////////
// Functions //
///////////////

/*
  Determines if two TADs are in contact with each other. The passed TAD locations are uses to determine the distance between the two TADs.
  We then grab the radius from the TAD vector. 
*/
bool TTMatrix::contact(TadLocation a, TadLocation b)
{
    double dist = sqrt(pow(b.x - a.x, 2)+pow(b.y - a.y, 2)+pow(b.z - a.z, 2)); //Distance formula
    double rad1 = tadVector[a.num]; //Get radius of tad a
    double rad2 = tadVector[b.num]; //Get radius of tad b
    if (dist < rad1+rad2)
    {
	return true;
    }
    return false;
}


/*
  Prints the heatmap matrix, should be piped to file to plot upon using gnuplot or etc.
*/
void TTMatrix::printMatrix()
{
    for (auto& row:finalMat)
    {
	for(auto& col:row)
	{
	    if (&col == &row.front())
	    {
		std::cout << col;
		continue;
	    }
	    std::cout << ", " << col; //Print out element in each row
	}
	std::cout << std::endl;
    }
}

    
/*
  Parses through the .vtf provided and constructs a matrix of size n x n where n is the number of TADS.
*/
void TTMatrix::parseVTF(std::ifstream& inFile)
{
    std::string line; //line in file
    //Parse vtf and load all the TADs and their radii into tadVector
    for (line; getline(inFile,line);) //Iterate over lines in file
    {
	std::vector<std::string> sLine; //split line
	boost::split(sLine, line, boost::is_any_of(" ")); //split on spaces
	/*
	  sLine is in format of atom tadnumValue:tadnumValue radius radiusValue name nameValue type typeValue q qValue
	  Example: atom 676,839 radius 0.187813 name X type 38 q 2.0
	*/
	if (!sLine[0].compare("pbc")) //we want to skip the first line (pbc = periodic boundary conditions)
	{
	    continue;
	}
	//Let's store the atoms and their radii

	//Check if we are done parsing atoms, a split size of less than 8 means we have parsed through all the TADs and their radii in the vtf.
	if (sLine.size() < 8)
	{
	    break;
	}
	else
	{
	    std::string tadGrouping = sLine[1];
	    std::vector<std::string> tads; //will split up tadnumValue into seperate TADs to be parsed
	    boost::split(tads,tadGrouping,boost::is_any_of(","));
	    //Go through each tad found within the line we are parsing
	    for (size_t i = 0; i < tads.size(); i++)
	    {
		//Two cases, when the tad is just singular like [483] vs when the tad is a range like [483:489]
		std::string currentTad = tads[i];
		size_t colonIndex = currentTad.find(':'); //Colon is the delimiter that lets us determine which case we are looking at
		if (colonIndex != std::string::npos)
		{
		    //A range of tads, not just a single tad!
		    int start     = std::atoi(currentTad.substr(0,colonIndex).c_str());
		    int end       = std::atoi(currentTad.substr(colonIndex+1).c_str());
		    double radius = std::atof(sLine[3].c_str()); //Precompute as it will be the same for the entire range
		    for (int i = start; i <= end;i++)
		    {
			tadMap[i] = radius;
		    }
		}
		else
		{
		    //Just a single tad
		    tadMap[std::atoi(currentTad.c_str())] = atof(sLine[3].c_str());
		}
	    }
	}
    }

    std::size_t tadCount = tadMap.size();
    tadVector.reserve(tadCount); //preallocate

    //convert tadMap to array so it is faster (by a large margin!)
    for (auto it = tadMap.begin(); it != tadMap.end(); ++it) //++it is faster than it++
    {
	tadVector.emplace_back(it->second);
    }
  
    std::vector<TadLocation> tadLocationVector; //Vector containing the xyz location of TADs in current snapshot
    finalMat.resize(tadCount, std::vector<double>(tadCount)); //dynamically allocate a 2D matrix
    int timeStepCount = 0; //The number of timestep within the vtf
    for (std::string line; getline(inFile,line);)
    {
	if (line.find("timestep") == std::string::npos)
	{
	    //Keep going to skip over atom and bond information we don't need
	    continue;
	}
	//At a snapshot segment in .vtf
	timeStepCount++;

	//Go over a snapshot and populate the tadLocationVector
	for (size_t i = 0; i < tadCount; i++)
	{
	    std::string tempLine;
	    getline(inFile,tempLine);
	    std::vector<std::string> sLine;
	    boost::split(sLine,tempLine,boost::is_any_of(" "));
	    TadLocation tempTadLoc;
	    tempTadLoc.init(i,std::stod(sLine[0]),std::stod(sLine[1]), std::stod(sLine[2]));
	    tadLocationVector.push_back(tempTadLoc);
	}
      
	//Parse the tadLocationVector
	for (size_t i = 0; i < tadCount; i++)
	{
	    for (size_t j = i; j < tadCount; j++)
	    {
		if (TTMatrix::contact(tadLocationVector[i],tadLocationVector[j]))
		{
		    //If in contact enter 1 into matrix
		    finalMat[i][j]++;
		}
	    }
	}
	tadLocationVector.clear(); //clear every iteration and reuse (space allocated is not destroyed)
    }
    //Divide the matrix by the number of snapshots to get the average contact value
    //Also copies the data along the diagonal due to nature of the heatmap
    for (size_t i = 0; i < tadCount; i++)
    {
	for (size_t j = i; j < tadCount; j++)
	{
	    finalMat[i][j] = finalMat[i][j]/timeStepCount;
	    finalMat[j][i] = finalMat[i][j];
	}
    }
}

int main(int argc, char* argv[])
{
    int c;
    char *fileName = NULL;
    //Get filename from command line parameters
    while((c = getopt(argc,argv,"f:")) != -1)
    {
	switch(c)
	{
	case 'f':
	    fileName = optarg;
	    break;
	default:
	    break;
	}
    }
    
    std::string line; //input line
    std::ifstream inFile; //input file
    inFile.open(fileName); //vtf file to parse
    //Error checking
    if (!inFile)
    {
	std::cout << "Can't open the vtf!";
	exit(1);
    }

    TTMatrix ttMatrix;
    ttMatrix.parseVTF(inFile);
    ttMatrix.printMatrix(); //prints to stdout, need to pipe to a file to make use of output
}



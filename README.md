# Table of Contents

1.  [Installing the plugin](#org8666318)
2.  [Basic usage tutorial](#org17af153)
    1.  [Dependencies:](#org92ec3d6)
    2.  [Loading input files ](#orgf6a5cdd)
    3.  [Viewing pairs ](#org5e57570)
    4.  [Exploring TADs ](#orgb77552b)
    5.  [Using Additional Features ](#org8a1517f)
3.  [Generate Input Files](#orgd042bea)
    1.  [Dependencies](#orgc1c8f88)
    2.  [To generate input files, look under `scripts/`.](#orgd868040)
4.  [Information to Note](#orga988752)


<a id="org8666318"></a>

# Installing the plugin

1.  Download and extract the plugin.
2.  Place the plugin (`tadview1.1/`) in a folder in your home directory named "vmdPlugins" that will house VMD plugins.
3.  The VMD startup configuration file must be modified to install the plugin.
4.  On Windows, append this line to `"%USERPROFILE%/vmd.rc"`.
    `set auto_path [linsert $auto_path 0 {C:\vmdPlugins}]`
5.  On Linux/Mac, append this line to `"$HOME/.vmdrc"`.
    `set auto_path [linsert $auto_path 0 [file join $env(HOME) vmdPlugins]]`
6.  Also append this line for access to the VMD main menu.
    `menu main on`
7.  Finally, install the plugin by appending this line to the same startup file.
    `vmd_install_extension tadview tadview_tk "TadView"`  
    "TadView" should now appear in VMD under Extensions &rarr; TadView  
    ![img](Images/Installing the plugin/Screenshot_20180816_135913_2018-08-16_13-59-17.png)  


<a id="org17af153"></a>

# Basic usage tutorial


<a id="org92ec3d6"></a>

## Dependencies:

-   gnuplot (optional)


<a id="orgf6a5cdd"></a>

## Loading input files <a id="org4bb7ae6"></a>

In order to make full use of this plugin, make sure to load the appropriate input data files. Sample input is provided in `sampleInput/`

-   **TAD Data Table:** `sampleDataTable.txt`
-   **Load Trajectory:** `sampleTrajectory.vtf`
-   **Load Pair Data Table:** `samplePairData.txt`
-   **Load Directory with Hi-C map matrices:** `matrices/`

![img](Images/Basic usage tutorial/Screenshot_20180816_140017_2018-08-16_14-00-20.png)  
To generate input files, refer to [Generate Input Files](#orgd042bea)


<a id="org5e57570"></a>

## Viewing pairs <a id="org33d5a11"></a>

![img](Images/Basic usage tutorial/Screenshot_20180817_170249_2018-08-17_17-02-50.png)  
"View Pairs" allows for the visualization of pairs found in the pair data table.
An example workflow with "View Pairs" would consist of:

1.  Go to "Find Pairs".
2.  Enter a chromosome name and the number of pairs desired.  
    Example:  
    `Chromosome = 2L`  
    `Number of pairs to display = 10`
3.  Click the "Generate pairs" button. The previously white box under "Pairs" should now be populated. You may have to scroll down to see the newly generated pairs.
    Notice that the format for each pair is the first TAD, second TAD, and contact frequency.
4.  Now click "Highlight pair". The pair should be visible now. The description for each highlighted pair will be colored red.
5.  Click on "Unhighlight pair" if desired.  
    ![img](Images/Basic usage tutorial/Screenshot_20180817_170433_2018-08-17_17-04-36.png)  
    The highlighted pair consists of the gray and red spheres to the middle left. Colors are based off of the epigentic class. The color mapping can be found [here](#orgc84b673)
6.  "Delete pair" deletes a pair from the list of pairs. Select a pair to delete by clicking on a pair and then clicking the "Delete pair" button.
7.  "Delete all pairs" will delete all pairs from the list of pairs.
8.  Click on "View Hi-C map" and choose a Hi-C map matrix.  
    Example: `matrix/2L_matrix.txt`  
    A Gnuplot window will pop up with a Hi-C map.

![img](Images/Basic usage tutorial/Screenshot_20180816_140558_2018-08-16_14-06-09.png)

1.  Custom pairs can also be added to the pair list under "Custom Pairs". Pairs not in the pair data table will not be added to the pair list.


<a id="orgb77552b"></a>

## Exploring TADs <a id="org76aafe8"></a>

"Explore TADs" quickly retrieves information about a TAD.
An example workflow with "Explore TADs" would consist of:

1.  Go to the entry box to the right of "TAD ID"
2.  Enter a TAD ID and click the "Add TAD" button immediately below the "TAD ID" entry.
3.  The "Selected TADs" box should be populated.
4.  Mouse over to the "Selected TADs" box and click on a TAD.
5.  The "Associated Genes" box should now be populated with the genes within the TAD. The "Related TADs" box may also be populated.  
    ![img](Images/Basic usage tutorial/Screenshot_20180816_140737_2018-08-16_14-07-39.png)  
    The TADs are colored with regard to their epigentic class. <a id="orgc84b673"></a>
    -   **Null:** Black
    -   **Active:** Red
    -   **PcG:** Blue
    -   **HP1_Centromeric:** Purple
6.  Clicking on a TAD in the "Related TADs" box will insert the clicked TAD to the "Selected TADs" box. This allows for the quick exploration of TADs.
7.  Delete a TAD from the "Selected TADs" box by first clicking on the TAD to delete and then clicking the "Delete TAD" button. Alternatively, click the "Delete All TADs" to start anew.


<a id="org8a1517f"></a>

## Using Additional Features <a id="orgca78e29"></a>

"Additional Features" provides some visualization settings to quickly change the representation in VMD.

-   **Show membrane:** Display the nuclear envelope/membrane.
-   **Slider bar:** Control the visibility of the nuclear envelop.
-   **Show chromosome arms:** Toggle a representation that colors the molecule based on the chromosome arm.
-   **Show territories:** Toggle a representation that shows semi-transparent chromosomal territories.
-   **Show LADs:** Toggle a representation that shows LADs. LADs will be in blue.


<a id="orgd042bea"></a>

# Generate Input Files


<a id="orgc1c8f88"></a>

## Dependencies

-   **`dataBaseGen.py`:** Python 3, pandas
-   **`pairGen.py`:** Python 3, pandas
-   **`submatrix.py`:** Python 3, pandas
-   **`tad_tad.cpp`:** gcc, boost


<a id="orgd868040"></a>

## To generate input files, look under `scripts/`.

-   **dataBaseGen:** Generates the TAD data table. Refer to the header of the script for further information.
-   **pairGen:** Generates the Pair data table. Refer to the header of the script for more information.
-   **submatrix:** Generates sub matrices for each chromosome. Refer to the header of the script for more information
-   **tadContact:** This generates a contact matrix that can be viewed with Gnuplot or the TadView plugin. Refer to the header of `tad_tad.cpp` for more information.

Sample inputs for the scripts can be found in the `sampleInput/` or within the directory of the script.


<a id="orga988752"></a>

# Information to Note

The pair data table is generated using the contact matrix of a trajectory. The TAD data table is generated using data from [Alber et. al](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5576134/) and [Sexton et. al](https://www.cell.com/cell/fulltext/S0092-8674(12)00016-5?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS0092867412000165%3Fshowall%3Dtrue). Thus, pairs found in [Viewing Pairs](#org33d5a11) may not directly correspond with the information displayed in [Exploring TADs](#org76aafe8).

# Funding
This work was supported by NSF grant MCB-1715207 to IVS and AVO, the graduate program in Genomics Bioinformatics and Computational Biology (GBCB) of Virginia Tech to NAK, by the Fralin Life Science Institute, the USDA National Institute of Food and Agriculture Hatch project 223822 to IVS. The funders had no role in study design, data collection, and analysis, decision to publish, or preparation of the manuscript.

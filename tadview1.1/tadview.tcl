###################
# TAD Viewer 1.1 #
###################
package provide tadview 1.1

namespace eval ::TadView:: {
    namespace export tadview
    # window handles
    variable w                                         ;# handle to main window

    # Input file names
    variable databaseFileName                          ;# name of the database file
    variable vtfFileName                               ;# name of vtf file
    variable pairFileName                              ;# name of pair file
    variable heatmapConfigFileName                     ;# name of gnuplot heatmap config
    variable heatmapMatrixDir "~"                      ;# dir with all the heatmap matrix input files
    variable smoothWidth 15                            ;# parameter to smooth rep
    variable tmpDir                                    ;# Temp dir to store gnuplot input files


    # Pair View Variables
    variable repDict                                   ;# dictionary containing representations
    variable selectedChr                               ;# name of chromosome to highlight, tied to text box which takes chromosome name as input
    variable numPairs                                  ;# number of pairs to show
    variable cp1                                       ;# custom pair #1
    variable cp2                                       ;# custom pair #2
    variable tadToAdd                                  ;# TAD to add in expl
    variable NEToggled                                 ;# Stores if the NE is toggled or not
    variable chrArmsToggled 1                          ;# Chromosome arm toggle variable
    variable terToggled                                ;# Territory toggle variable
    variable ter
    variable armReps                                   ;# Stores the reps for the default representation
    variable clipPlaneScale                            ;# Stores the clip plane scale
    variable epiClassColorDict [dict create Null [list 2 black] Active [list 1 red] HP1_centromeric [list 11 purple] PcG [list 0 blue]]
    variable chrList {{2L 0,254 9} {2R 255,437 1} {3L 438,658 15} {3R 659,965 0} {4 966,984 7} {X 985,1168 31}}
}

#
# Create the window and initialize data structures
#
proc ::TadView::tadview {} {
    variable w
    variable textfile
    # If already initialized, just turn on
    if { [winfo exists .tadview] } {
        wm deiconify $w
        return
    }

    set w [toplevel ".tadview"]
    wm title $w "TAD Viewer"
    wm resizable $w 1 1

    ttk::notebook $w.n
    ttk::frame $w.n.load; #Loader
    ttk::frame $w.n.pairs;#Pair Viewer
    ttk::frame $w.n.expl; #Explorer
    ttk::frame $w.n.vis;  #Visualizer

    # Load
    button $w.n.load.db -text "Load TAD data table" -command {::TadView::loadFile "::TadView::databaseFileName" "Select TAD data table file"}
    button $w.n.load.vtf -text "Load trajectory" -command {::TadView::loadVTF "::TadView::vtfFileName" "Select VTF file"}
    button $w.n.load.pair -text "Load pair data table" -command {::TadView::loadFile "::TadView::pairFileName" "Select pair data table file"}
    button $w.n.load.heatmap -text "Load directory with Hi-C map matrices" -command {::TadView::loadHeatmapDir "::TadView::heatmapMatrixDir" "Select directory with heatmap matrices"}
    pack $w.n.load.db -expand yes -fill both
    pack $w.n.load.vtf -expand yes -fill both
    pack $w.n.load.pair -expand yes -fill both
    pack $w.n.load.heatmap -expand yes -fill both

    # Pair View
    labelframe $w.n.pairs.pairFrame -text "Pairs"
    labelframe $w.n.pairs.findPairsFrame -text "Find Pairs"
    labelframe $w.n.pairs.customPairsFrame -text "Custom Pairs"

    listbox $w.n.pairs.pairFrame.pairBox
    button $w.n.pairs.pairFrame.hl -text "Highlight pair" -command "::TadView::highlightPairWrapper $w.n.pairs.pairFrame.pairBox"
    button $w.n.pairs.pairFrame.uhl -text "Unhighlight pair" -command "::TadView::unhighlightPairWrapper $w.n.pairs.pairFrame.pairBox"
    button $w.n.pairs.pairFrame.delPairs -text "Delete pair" -command "::TadView::deletePair $w.n.pairs.pairFrame.pairBox"
    button $w.n.pairs.pairFrame.delAllPairs -text "Delete all pairs" -command "::TadView::deleteAllPairs $w.n.pairs.pairFrame.pairBox"
    button $w.n.pairs.pairFrame.heatmap -text "View Hi-C map" -command "::TadView::showHeatmap"

    # Find pairs
    label $w.n.pairs.findPairsFrame.chrEntryLabel -text "Chromosome"
    entry $w.n.pairs.findPairsFrame.chrEntry -textvariable ::TadView::selectedChr; #chr to select
    label $w.n.pairs.findPairsFrame.numEntryLabel -text "Number of pairs to display"
    entry $w.n.pairs.findPairsFrame.numEntry -textvariable ::TadView::numPairs; #num of pairs to show
    button $w.n.pairs.findPairsFrame.generatePairs -text "Generate pairs" -command "::TadView::generatePairs $w.n.pairs.pairFrame.pairBox"

    # Custom pairs
    label $w.n.pairs.customPairsFrame.customPair1 -text "TAD ID 1"
    entry $w.n.pairs.customPairsFrame.cp1Entry -textvariable ::TadView::cp1; # Custom TAD 1
    label $w.n.pairs.customPairsFrame.customPair2 -text "TAD ID 2"
    entry $w.n.pairs.customPairsFrame.cp2Entry -textvariable ::TadView::cp2; # Custom TAD 2
    button $w.n.pairs.customPairsFrame.addPair -text "Add pair" -command "::TadView::addPair $w.n.pairs.pairFrame.pairBox"

    # Gridding higher level frames
    grid $w.n.pairs.pairFrame -column 1 -row 1 -rowspan 2 -sticky news
    grid $w.n.pairs.findPairsFrame -column 2 -row 1 -sticky news
    grid $w.n.pairs.customPairsFrame -column 2 -row 2 -sticky news
    grid columnconfigure $w.n.pairs {1} -weight 1
    grid rowconfigure $w.n.pairs {1 2} -weight 1


    # Gridding pair frame
    grid $w.n.pairs.pairFrame.pairBox -column 1 -row 1 -sticky nsew
    grid $w.n.pairs.pairFrame.hl -column 1 -row 2 -sticky nsew
    grid $w.n.pairs.pairFrame.uhl -column 1 -row 3 -sticky nsew
    grid $w.n.pairs.pairFrame.delPairs -column 1 -row 4 -sticky nsew
    grid $w.n.pairs.pairFrame.delAllPairs -column 1 -row 5 -sticky nsew
    grid $w.n.pairs.pairFrame.heatmap -column 1 -row 6 -sticky nsew
    grid columnconfigure $w.n.pairs.pairFrame 1 -weight 1
    grid rowconfigure $w.n.pairs.pairFrame {1} -weight 20
    grid rowconfigure $w.n.pairs.pairFrame {2 3 4 5 6} -weight 1

    #Gridding find pairs frame
    grid $w.n.pairs.findPairsFrame.chrEntryLabel -column 1 -row 1
    grid $w.n.pairs.findPairsFrame.chrEntry -column 1 -row 2
    grid $w.n.pairs.findPairsFrame.numEntryLabel -column 2 -row 1
    grid $w.n.pairs.findPairsFrame.numEntry -column 2 -row 2
    grid $w.n.pairs.findPairsFrame.generatePairs -column 1 -columnspan 2 -row 3 -sticky nsew
    grid rowconfigure $w.n.pairs.findPairsFrame {3} -weight 1
    grid columnconfigure $w.n.pairs.findPairsFrame {1} -weight 1

    #Gridding custom pairs frame
    grid $w.n.pairs.customPairsFrame.customPair1 -column 1 -row 1
    grid $w.n.pairs.customPairsFrame.cp1Entry -column 1 -row 2
    grid $w.n.pairs.customPairsFrame.customPair2 -column 2 -row 1
    grid $w.n.pairs.customPairsFrame.cp2Entry -column 2 -row 2
    grid $w.n.pairs.customPairsFrame.addPair -column 1 -columnspan 2 -row 3 -sticky nsew
    grid rowconfigure $w.n.pairs.customPairsFrame {3} -weight 1
    grid columnconfigure $w.n.pairs.customPairsFrame {1} -weight 1

    # TAD explorer
    listbox $w.n.expl.tadBox
    listbox $w.n.expl.geneBox
    listbox $w.n.expl.relatedTadBox
    label   $w.n.expl.tadBoxLabel -text "Selected TADs"
    label   $w.n.expl.geneBoxLabel -text "Associated Genes"
    label   $w.n.expl.relatedTadBoxLabel -text "Related TADs"
    entry   $w.n.expl.addTadEntry -textvariable ::TadView::tadToAdd
    label   $w.n.expl.addTadEntryLabel -text "TAD ID"
    button  $w.n.expl.addTad -text "Add TAD" -command "::TadView::addTadWrapper $w.n.expl.tadBox"
    button  $w.n.expl.delTad -text "Delete TAD" -command "::TadView::deleteTad $w.n.expl.tadBox $w.n.expl.geneBox $w.n.expl.relatedTadBox"
    button  $w.n.expl.delAllTad -text "Delete all TADs" -command "::TadView::deleteAllTads $w.n.expl.tadBox $w.n.expl.geneBox $w.n.expl.relatedTadBox"
    bind $w.n.expl.tadBox <<ListboxSelect>> [list ::TadView::tadSelectHandler $w.n.expl.tadBox $w.n.expl.geneBox $w.n.expl.relatedTadBox]
    bind $w.n.expl.relatedTadBox <<ListboxSelect>> [list ::TadView::relatedTadSelectHandler $w.n.expl.tadBox $w.n.expl.relatedTadBox]

    grid $w.n.expl.tadBoxLabel -column 1 -row 1
    grid $w.n.expl.tadBox -column 1 -row 2 -rowspan 10 -sticky news
    grid $w.n.expl.geneBoxLabel -column 2 -row 1
    grid $w.n.expl.geneBox -column 2 -row 2 -rowspan 10 -sticky news
    grid $w.n.expl.relatedTadBoxLabel -column 3 -row 1
    grid $w.n.expl.relatedTadBox -column 3 -row 2 -rowspan 10 -stick news
    grid $w.n.expl.addTadEntry -column 2 -row 15 -sticky ew
    grid $w.n.expl.addTadEntryLabel -column 1 -row 15 -sticky e
    grid $w.n.expl.addTad -column 2 -row 16 -sticky news
    grid $w.n.expl.delTad -column 2 -row 18 -sticky news
    grid $w.n.expl.delAllTad -column 2 -row 20 -sticky news
    grid columnconfigure $w.n.expl  {1 2 3} -weight 1
    grid rowconfigure $w.n.expl {1 2 3 4 5 6 7 8 9 10 11 12} -weight 1

    # Visualization Settings
    checkbutton $w.n.vis.ne -text "Show nuclear membrane" -variable ::TadView::NEToggled -command {if {$::TadView::NEToggled} {
        mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "NE"]] 1;
    } else {
        mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "NE"]] 0;
    }}
    scale $w.n.vis.neScale -from -5.0 -to 5.0  -resolution 0.1 -digits 6 -orien horizontal -variable "::TadView::clipPlaneScale" -command "::TadView::setClipPlane $w.n.vis.neScale"
    checkbutton $w.n.vis.lad -text "Show LADs" -variable ::TadView::LADToggled -command {if {$::TadView::LADToggled} {
        ::TadView::showLADRep
    } else {
        ::TadView::hideLADRep
    }}
    checkbutton $w.n.vis.ter -text "Show territories" -variable ::TadView::terToggled -command {if {$::TadView::terToggled} {
        ::TadView::showTer
    } else {
        ::TadView::hideTer
    }}
    checkbutton $w.n.vis.arms -text "Show chromosome arms" -variable ::TadView::chrArmsToggled -command {if {$::TadView::chrArmsToggled} {
        ::TadView::showArmsRep
    } else {
        ::TadView::hideArmsRep
    }}

    pack $w.n.vis.ne
    pack $w.n.vis.neScale
    pack $w.n.vis.arms
    pack $w.n.vis.ter
    pack $w.n.vis.lad

    $w.n add $w.n.load -text "Load Files"
    $w.n add $w.n.pairs -text "View Pairs"
    $w.n add $w.n.expl -text "Explore TADs"
    $w.n add $w.n.vis -text "Additional Features"

    pack $w.n -expand yes -fill both
    ::TadView::initTempDir
}

proc ::TadView::addTad {tadBox selTad} {
    variable epiClassColorDict
    if {$selTad eq ""} {
        return
    }
    set tadInfo [getTadInfo $selTad]
    if {$tadInfo eq "None"} {
        return
    } else {
        set chr [lindex $tadInfo 0]
        set epiClass [lindex $tadInfo 1]
        set geneList [lindex $tadInfo 2]
        set relatedTadList [lindex $tadInfo 3]
        $tadBox insert end "TAD $selTad"
        set color [lindex [dict get $epiClassColorDict $epiClass] 1]
        $tadBox itemconfigure end -background $color -foreground white -selectforeground yellow -selectbackground $color
    }
}

proc ::TadView::addTad2 {tadBox selTad} {
    upvar $selTad tadToAdd
    variable epiClassColorDict
    if {$tadToAdd eq ""} {
        return
    }
    set tadInfo [getTadInfo $tadToAdd]
    if {$tadInfo eq "None"} {
        return
    } else {
        set chr [lindex $tadInfo 0]
        set epiClass [lindex $tadInfo 1]
        set geneList [lindex $tadInfo 2]
        set relatedTadList [lindex $tadInfo 3]
        $tadBox insert end "TAD $tadToAdd"
        set color [lindex [dict get $epiClassColorDict $epiClass] 1]
        $tadBox itemconfigure end -background $color -foreground white -selectforeground yellow -selectbackground $color
    }


}

proc ::TadView::deleteTad {tadBox geneBox relatedTadBox} {
    $geneBox delete 0 [expr [$geneBox index end] - 1]
    $relatedTadBox delete 0 [expr [$relatedTadBox index end] - 1]
    if {[$tadBox curselection] eq ""} {
        return
    }

    set selectedTadID [lindex [split [$tadBox get [$tadBox curselection]] " "] end];
    $tadBox delete [lsearch [$tadBox get 0 end] "*$selectedTadID*"]
}

proc ::TadView::deleteAllTads {tadBox geneBox relatedTadBox} {
    $geneBox delete 0 [expr [$geneBox index end] - 1]
    $relatedTadBox delete 0 [expr [$relatedTadBox index end] - 1]
    $tadBox  delete 0 [expr [$tadBox  index end] - 1]; #Delete all
}

proc ::TadView::tadSelectHandler {tadBox geneBox relatedTadBox} {
    variable epiClassColorDict
    $geneBox delete 0 [expr [$geneBox index end] - 1]
    $relatedTadBox delete 0 [expr [$relatedTadBox index end] - 1]
    if {[$tadBox curselection] eq ""} {
        return
    }

    set selectedTadID [lindex [split [$tadBox get [$tadBox curselection]] " "] end]; #get the selected TAD id from the listbox
    set tadInfo [getTadInfo $selectedTadID]; # Contains relevant info for the selected TAD
    set geneList [lindex $tadInfo 2]
    set relatedTadList [lindex $tadInfo 3]
    # Populate the "Associated Genes" box
    foreach gene $geneList {
        $geneBox insert end $gene
    }

    # Populate the "Related TADs" box
    foreach relatedTad $relatedTadList {
        set tadInfo [getTadInfo $relatedTad]; # Contains relevant info for the selected TAD
        set chr [lindex $tadInfo 0]
        set epiClass [lindex $tadInfo 1]
        set geneList [lindex $tadInfo 2]
        set relatedTadList [lindex $tadInfo 3]

        $relatedTadBox insert end "TAD $relatedTad"
        set color [lindex [dict get $epiClassColorDict $epiClass] 1]
        $relatedTadBox itemconfigure end -background $color -foreground white -selectforeground yellow -selectbackground $color
    }
}

proc ::TadView::relatedTadSelectHandler {tadBox relatedTadBox} {
    variable relDict
    variable numLabels

    if {[$relatedTadBox curselection] eq ""} {
        return
    }

    set selectedRelatedTads [lindex [split [$relatedTadBox get [$relatedTadBox curselection]] " "] end]
    foreach relatedTad $selectedRelatedTads {
        addTad $tadBox $relatedTad
    }
}

proc ::TadView::addTadWrapper {tadBox} {
    variable tadToAdd
    addTad $tadBox $tadToAdd
}


proc ::TadView::initTempDir {} {
    variable tmpDir
    # http://wiki.tcl.tk/772
    set tmpDir [pwd]
    if {[file exists "/tmp"]} {set tmpDir "/tmp"}
    catch {set tmpDir $::env(TRASH_FOLDER)} ;# very old Macintosh. Mac OS X doesn't have this.
    catch {set tmpDir $::env(TMP)}
    catch {set tmpDir $::env(TEMP)}
}

proc ::TadView::getTadInfo {tadID} {
    variable databaseFileName

    set f [open $databaseFileName r]; #TODO make this flexible
    set lines [split [read $f] "\n"]
    close $f;
    set firstLineFlag 0
    foreach line $lines {
        if {$firstLineFlag == 0} { ; #skip header line in csv
            set firstLineFlag 1
            continue
        }
        set splitLine [split $line "\t"]
        #find the related tad
        if {[lindex $splitLine 0] == $tadID} {
            set chr [lindex $splitLine 1]
            set epiClass [lindex $splitLine 2]
            set geneList [split [lindex $splitLine end-1] ", "]
            set relatedTadList [split [lindex $splitLine end] ", "]
            return [list $chr $epiClass $geneList $relatedTadList]
        }
    }
    return "None"

}

proc ::TadView::loadVTF {fileVar title} {
    variable smoothWidth
    upvar $fileVar filePath
    set filePath [tk_getOpenFile -title $title];
    mol new $filePath
    mol delrep 0 0
    ::TadView::initNE
    ::TadView::initArmsRep
    ::TadView::initTerRep
    ::TadView::initLADRep
    ::TadView::showArmsRep
}

proc ::TadView::showNERep {} {
    variable chrList
    variable repDict
    mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "NE"]] 1;
}

proc ::TadView::hideNERep {} {
    variable chrList
    variable repDict
    mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "NE"]] 0;
}

proc ::TadView::hideMajorReps {} {
    ::TadView::hideArmsRep
    ::TadView::hideNERep
    ::TadView::hideTer
}

proc ::TadView::showTer {} {
    variable chrList
    variable repDict
    foreach chr $chrList {
        set splitLine [split $chr " "]
        set chrName [lindex $splitLine 0]
        mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "${chrName}_t"]] 1;
    }
}

proc ::TadView::hideTer {} {
    variable chrList
    variable repDict
    foreach chr $chrList {
        set splitLine [split $chr " "]
        set chrName [lindex $splitLine 0]
        mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "${chrName}_t"]] 0;
    }
}

proc ::TadView::initTerRep {} {
    variable repDict
    variable chrList
    variable smoothWidth

    foreach chr $chrList {
        # Ex chr: "chrName range colorID" -> "2L 0,255 1"
        set splitLine [split $chr " "]
        set chrRange [split [lindex $splitLine 1] ","]
        mol addrep 0
        set numReps [molinfo top get numreps]
        set repNumber [expr {$numReps-1}]
        set repName [mol repName 0 $repNumber]
        mol modselect $repNumber 0 index [lindex $chrRange 0] to [lindex $chrRange 1]
        mol modcolor $repNumber 0 "ColorID" [lindex $splitLine 2]
        mol modmaterial $repNumber 0 Transparent
        mol modstyle $repNumber 0 QuickSurf 6.00000 20.000000 0.500000 1.000000
        mol smoothrep 0 $repNumber $smoothWidth
        mol showrep 0 $repNumber 0; # Hide the representation
        dict set repDict "[lindex $splitLine 0]_t" $repName; #Name the rep based on chr name
    }
}

proc ::TadView::showArmsRep {} {
    variable chrList
    variable repDict
    foreach chr $chrList {
        set splitLine [split $chr " "]
        set chrName [lindex $splitLine 0]
        mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict $chrName]] 1;
    }
}

proc ::TadView::hideArmsRep {} {
    variable chrList
    variable repDict
    foreach chr $chrList {
        set splitLine [split $chr " "]
        set chrName [lindex $splitLine 0]
        mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict $chrName]] 0;
    }
}

# Sets the filename path to the variable with the name of fileVar.
proc ::TadView::loadFile {fileVar title} {
    upvar $fileVar filePath
    set filePath [tk_getOpenFile -title $title]

}

proc ::TadView::loadHeatmapDir {dirVar title} {
    upvar $dirVar dirPath
    set dirPath [tk_chooseDirectory -title $title]
}


proc ::TadView::showHeatmap {} {
    variable heatmapMatrixDir
    variable tmpDir
    set matrixPath [tk_getOpenFile -title "Choose a Hi-C map matrix to plot." -initialdir $heatmapMatrixDir]
    if {$matrixPath ne ""} {
        set fileName [file join $tmpDir [pid]]
        set fileP [open $fileName w]
        puts $fileP "set autoscale xfix; set autoscale yfix\nset palette defined ( 0 \"white\", 1 \"red\", 2 \"dark-red\")\nset datafile separator \",\"\nplot '$matrixPath' matrix with image notitle\npause -1"
        close $fileP
        exec gnuplot $fileName &
    }
}

proc ::TadView::deleteAllPairs {pairBox} {
    set size [$pairBox size]
    for {set i 0} {$i < $size} {incr i} {
        unhighlightPair $pairBox $i
    }
    $pairBox delete 0 [expr [$pairBox index end] - 1]
}


proc ::TadView::deletePair {pairBox} {
    variable repDict
    if {[$pairBox curselection] eq ""} {
        return
    }
    unhighlightPair $pairBox [$pairBox curselection]; # Unhighlight
    $pairBox delete [$pairBox curselection]
}

proc ::TadView::highlightPairWrapper {pairBox} {
    highlightPair $pairBox [$pairBox curselection]
}


proc ::TadView::highlightPair {pairBox pairToHighlight} {
    variable repDict
    variable smoothWidth
    variable epiClassColorDict

    if {$pairToHighlight eq ""} {
        return
    }
    # In the format of "TAD1 TAD2 PROBABILITY"
    set selectedPair [split [$pairBox get $pairToHighlight] " "];
    # Do nothing if already highlighted
    set first [lindex $selectedPair 0]
    set second [lindex $selectedPair 1]
    if {[dict exists $repDict $first] && [dict exists $repDict $second]} {
        return
    }
    $pairBox itemconfigure $pairToHighlight -foreground red -selectforeground red

    set pair [list $first $second]
    set repNames [list]
    foreach tadIdx $pair {
        #Make the representation
        mol addrep 0
        set numReps [molinfo top get numreps]
        set repNumber [expr {$numReps-1}]
        set repName [mol repName 0 $repNumber]

        set tadInfo [getTadInfo $tadIdx]
        set epiClass [lindex $tadInfo 1]

        mol modselect $repNumber 0 index [expr $tadIdx]; # offset by 1
        set color [lindex [dict get $epiClassColorDict $epiClass] 0]
        mol modcolor $repNumber 0 "ColorID" $color
        # Set representation style
        mol modstyle $repNumber 0 "VDW"
        mol modmaterial $repNumber 0 "Opaque"
        mol smoothrep 0 $repNumber $smoothWidth
        set repNames [lappend repNames $repName]
    }
    dict set repDict $first$second [lindex $repNames 0]
    dict set repDict $second$first [lindex $repNames 1]
    # labelTad $first
    # labelTad $second
}

proc ::TadView::unhighlightPairWrapper {pairBox} {
    unhighlightPair $pairBox [$pairBox curselection]
}


proc ::TadView::unhighlightPair {pairBox pairToUnhighlight} {
    variable repDict
    set selectedPair [split [$pairBox get $pairToUnhighlight] " "];
    if {$selectedPair eq ""} {
        return
    }
    $pairBox itemconfigure $pairToUnhighlight -foreground black -selectforeground black
    set first [lindex $selectedPair 0]
    set second [lindex $selectedPair 1]
    set repsToDel [list $first$second $second$first]
    foreach tad $repsToDel {
        if {[dict exists $repDict $tad]} {
            mol delrep [mol repindex 0 [dict get $repDict $tad]] 0
            set repDict [dict remove $repDict $tad]
        }
    }
}

proc ::TadView::addPair {pairBox} {
    variable cp1
    variable cp2
    variable pairFileName

    set f [open $pairFileName r]
    set lines [split [read $f] "\n"]
    close $f
    foreach line $lines {
        set splitLine [split $line "\t"]
        set tad1 [lindex $splitLine 0]
        set tad2 [lindex $splitLine 1]
        if {([string equal $tad1 $cp1] && [string equal $tad2 $cp2]) ||  ([string equal $tad2 $cp1] && [string equal $tad1 $cp2])} {
            $pairBox insert end [join [lrange $splitLine 0 2]] ; # Do not insert the chr
        }
    }
}

proc ::TadView::setClipPlane {tclWidget something} {
    variable clipPlaneScale
    variable repDict
    if [catch {dict get $repDict "NE"}] {
        return
    } else {
        set NERepNum [mol repindex 0 [dict get $repDict "NE"]];
        mol clipplane status 0 $NERepNum 0 1; #turn on and off
        set center [lindex [molinfo 0 get center] 0]
        set offset 0
        set offset [lappend $offset $something $something $something]
        set clipVec [vecadd $center $offset]
        mol clipplane center 0 $NERepNum 0 $clipVec; # Change to use center from vtf
        mol clipplane normal 0 $NERepNum 0 {1 0 0}; # The direction of the clipplane
    }

}
# Initialize nuclear envelop
proc ::TadView::initNE {} {
    variable repDict
    #make NE
    mol addrep 0
    set numReps [molinfo top get numreps]
    set repNumber [expr {$numReps-1}]
    set repName [mol repName 0 $repNumber]
    # Nuclear envelop is set to be an extra bead in the vtf, so we get the last bead
    set membraneNum [expr [molinfo top get numatoms]-1]
    mol modselect $repNumber 0 index $membraneNum
    # Make representation red
    mol modcolor $repNumber 0 "ColorID" 2
    # Set representation style
    mol modstyle $repNumber 0 "VDW"
    mol modmaterial $repNumber 0 "Transparent"
    mol showrep 0 $repNumber 0; #Hide the representation
    dict set repDict "NE" $repName
}


proc ::TadView::initLADRep {} {
    variable repDict
    variable chrList
    variable smoothWidth

    mol addrep 0
    set numReps [molinfo top get numreps]
    set repNumber [expr {$numReps-1}]
    set repName [mol repName 0 $repNumber]
    mol modselect $repNumber 0 index 0 to [expr [molinfo top get numatoms]-2]
    mol modcolor $repNumber 0 Charge
    mol modselect $repNumber 0 index 0 to [expr [molinfo top get numatoms]-2]
    mol modstyle $repNumber 0 CPK 1.600000 0.000000 12.000000 12.000000
    mol smoothrep 0 $repNumber $smoothWidth
    mol showrep 0 $repNumber 0; #Hide the representation
    dict set repDict "LAD" $repName

}

proc ::TadView::showLADRep {} {
    variable chrList
    variable repDict
    mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "LAD"]] 1;
}

proc ::TadView::hideLADRep {} {
    variable chrList
    variable repDict
    mol showrep 0 [mol repindex 0 [dict get $::TadView::repDict "LAD"]] 0;
}


proc ::TadView::initArmsRep {} {
    variable repDict
    variable chrList
    variable smoothWidth
    # TCL dict chr and tuple

    foreach chr $chrList {
        # Ex chr: "chrName range colorID" -> "2L 0,255 1"
        set splitLine [split $chr " "]
        set chrRange [split [lindex $splitLine 1] ","]
        mol addrep 0
        set numReps [molinfo top get numreps]
        set repNumber [expr {$numReps-1}]
        set repName [mol repName 0 $repNumber]
        mol modselect $repNumber 0 index [lindex $chrRange 0] to [lindex $chrRange 1]
        mol modcolor $repNumber 0 "ColorID" [lindex $splitLine 2]
        mol modstyle $repNumber 0 Lines 3.0
        mol smoothrep 0 $repNumber $smoothWidth
        mol showrep 0 $repNumber 0; # Hide the representation
        dict set repDict [lindex $splitLine 0] $repName; #Name the rep based on chr name
    }
}

proc tadview_tk { } {
    TadView::tadview
    return $TadView::w
}

proc labelTad {tadID} {
    label add Atoms 0/$tadID
    label textformat Atoms [lsearch [label list Atoms] "*$tadID*"] {TAD %i}
}

proc unLabelTad {tadID} {
    label delete Atoms [lsearch [label list Atoms] "*$tadID*"]
}


proc ::TadView::generatePairs {pairBox} {
    variable numPairs
    variable selectedChr
    variable pairFileName

    set pairsFound 0
    set f [open $pairFileName]
    set lines [split [read $f] "\n"]
    close $f
    foreach line $lines {
        set splitLine [split $line "\t"]
        if ([string equal [lindex $splitLine 3] $selectedChr]) {
            $pairBox insert end [join [lrange $splitLine 0 2]] ; # Do not insert the chr
            incr pairsFound
        }
        if {$numPairs == $pairsFound} {
            return
        }
    }
}
